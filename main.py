import discord
import time
import os

from discord.ext import commands
from mutagen.mp3 import MP3

AUDIO_FILE = os.environ.get("WELCOME_FILE") or "data/welcome.mp3"

lenght_of_audio = MP3(AUDIO_FILE).info.length


class Music(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_voice_state_update(self, member, before, after):
        if member == self.bot.user:
            return
        if not after.channel:
            return
        guild = member.guild
        channel = self.bot.get_channel(after.channel.id)
        if guild.voice_client is not None:
            await guild.voice_client.move_to(channel)
        else:
            await channel.connect()
        source = discord.PCMVolumeTransformer(discord.FFmpegPCMAudio(AUDIO_FILE))
        guild.voice_client.play(source, after=lambda e: print('Player error: %s' % e) if e else None)
        time.sleep(lenght_of_audio + 1)
        await guild.voice_client.disconnect()


bot = commands.Bot(command_prefix=commands.when_mentioned_or("!"),
                   description='Relatively simple music bot example')


@bot.event
async def on_ready():
    print('Logged in as {0} ({0.id})'.format(bot.user))
    print('------')


bot.add_cog(Music(bot))
bot.run(os.environ["TOKEN"])
