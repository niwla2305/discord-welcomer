# Discord Welcomer
This bot plays an mp3 file when a member joins a voice channel.

## Installation
 * `git clone https://gitlab.com/Niwla23/discord-welcomer.git`
 * Modify `docker-compose.yml` to fit your token.
 * In the data directory put a mp3 file called `welcome.mp3`
 * Run `docker-compse up -d`
 * You are done! The bot should play the sound when someone joins a channel on your server.
